// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TopDownShooter/Interface/TPS_InterfaceGameActor.h"
#include "TopDownShooter/Items/Weapon/TPS_StateEffect.h"
#include "TPS_EnvironmentStructure.generated.h"

UCLASS()
class TOPDOWNSHOOTER_API ATPS_EnvironmentStructure : public AActor, public ITPS_InterfaceGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPS_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	//Effects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UTPS_StateEffect*> Effects;

	virtual TArray<UTPS_StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	virtual void AddEffect(UTPS_StateEffect* NewEffect);
};
