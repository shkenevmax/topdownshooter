// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_InterfaceGameActor.h"

// Add default functionality here for any ITPS_InterfaceGameActor functions that are not pure virtual.

EPhysicalSurface ITPS_InterfaceGameActor::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTPS_StateEffect*> ITPS_InterfaceGameActor::GetAllCurrentEffects()
{
	TArray<UTPS_StateEffect*> Effects;
	return Effects;
}

void ITPS_InterfaceGameActor::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{

}

void ITPS_InterfaceGameActor::AddEffect(UTPS_StateEffect* NewEffect)
{

}