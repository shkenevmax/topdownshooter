// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter/Game/TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - WeaponTable - NULL"));
		}
	}
	
	return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByWeaponName(FName NameWeapon, FDropWeapon& OutInfo)
{
	bool bIsFind = false;

	if (DropWeaponInfoTable)
	{
		FDropWeapon* DropWeaponInfoRow;
		TArray<FName> RowNames = DropWeaponInfoTable->GetRowNames();

		int32 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropWeaponInfoRow = DropWeaponInfoTable->FindRow<FDropWeapon>(RowNames[i], "");

			if (DropWeaponInfoRow->WeaponInfo.NameItem == NameWeapon)
			{
				OutInfo = (* DropWeaponInfoRow);
				bIsFind = true;
			}
			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetDropWeaponInfoByName - DropWeaponTable - NULL"));
	}

	return bIsFind;
}

bool UTDSGameInstance::GetDropItemInfoByName(FName NameWeapon, FDropWeapon& OutInfo)
{
	bool bIsFind = false;
	FDropWeapon* DropWeaponInfoRow;

	if (DropWeaponInfoTable)
	{
		DropWeaponInfoRow = DropWeaponInfoTable->FindRow<FDropWeapon>(NameWeapon, "", false);
		if (DropWeaponInfoRow)
		{
			OutInfo = *DropWeaponInfoRow;
			bIsFind = true;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetDropWeaponInfoByName - DropWeaponTable - NULL"));
	}

	return bIsFind;
}
