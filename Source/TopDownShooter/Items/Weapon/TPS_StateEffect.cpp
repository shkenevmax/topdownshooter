// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter/Items/Weapon/TPS_StateEffect.h"
#include "TopDownShooter/Character/TPSHealthComponent.h"
#include "TopDownShooter/Character/TopDownShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "TopDownShooter/Interface/TPS_InterfaceGameActor.h"

bool UTPS_StateEffect::InitObject(AActor* Actor, FName BoneHit)
{
    myActor = Actor;

    ITPS_InterfaceGameActor* myInterface = Cast<ITPS_InterfaceGameActor>(myActor);
    if (myInterface)
    {
        myInterface->AddEffect(this);
    }

    return true;
}

void UTPS_StateEffect::DestroyObject()
{
    ITPS_InterfaceGameActor* myInterface = Cast<ITPS_InterfaceGameActor>(myActor);
    if (myInterface)
    {
        myInterface->RemoveEffect(this);
    }

    myActor = nullptr;

    if (this && this->IsValidLowLevel())
    {
        this->ConditionalBeginDestroy();
    }
}

bool UTPS_StateEffect::CheckStakebleEffect()
{
    return false;
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName BoneHit)
{
    Super::InitObject(Actor, BoneHit);
    ExecuteOnce();
    return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
    Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
    if (myActor)
    {
        UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>
            (myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));

        if (myHealthComp)
        {
            myHealthComp->ChangeCurrentHealth(Power);
        }
    }
    DestroyObject();
}
    
bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName BoneHit)
{
    Super::InitObject(Actor, BoneHit);

    GetWorld()->GetTimerManager().SetTimer(ExecuteTimer,
        this, &UTPS_StateEffect_ExecuteTimer::Execute,
        RateTime, true);

    GetWorld()->GetTimerManager().SetTimer(EffectTimer,
        this, &UTPS_StateEffect_ExecuteTimer::DestroyObject,
        GlobalTimer, false);

    if (ParticleEffect)
    {
        FName nameBoneToAttached = BoneHit; // "head"
        FVector Loc;

        USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
        if (myMesh)
        {
            ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh,
                nameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
        }
        else
        {
            ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(),
                nameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
        }
    }

    CharStunned();
    BurstHealth();
    
    return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
    if (ParticleEmitter)
    {
        ParticleEmitter->DestroyComponent();
    }
    ParticleEmitter = nullptr;
    CharStunned();
    BurstHealth();

    Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
    if (myActor)
    {
        UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>
            (myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));

        if (myHealthComp)
        {
            myHealthComp->ChangeCurrentHealth(Power);
        }
    }
}

void UTPS_StateEffect_ExecuteTimer::CharStunned()
{
    ATopDownShooterCharacter* Char = Cast<ATopDownShooterCharacter>(myActor);
    if (Char)
    {
        if (bIsStuned)
        {
            Char->bIsAlive = false;
            Char->DisableInput(NULL);
            bIsStuned = false;

            // stun animation
            if (Char->StunAnim)
            {
                Char->PlayAnimMontage(Char->StunAnim);
            }
            else
            {
                UE_LOG(LogTemp, Warning, TEXT("UTPS_StateEffect_ExecuteTimer::CharStunned - no animations available for stun"));
            }
        }
        else
        {
            Char->bIsAlive = true;
            Char->StopAnimMontage(Char->StunAnim);
            Char->EnableInput(NULL);
        }
    }
}

void UTPS_StateEffect_ExecuteTimer::BurstHealth()
{
    if (myActor)
    {
        UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>
         (myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));

            if (myHealthComp)
            {
                if (bIsHealthBursted)
                {
                    myHealthComp->SetMaxHealth(120.0f);
                    bIsHealthBursted = false;
                    if (ParticleEffect)
                    {
                        FName nameBoneToAttached = "head";
                        FVector Loc;
                        ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(),
                            nameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
                    }
                }
                else
                {
                    myHealthComp->SetMaxHealth(100.0f);
                }
            }
      
    }
}
