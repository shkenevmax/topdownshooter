// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TPS_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TOPDOWNSHOOTER_API UTPS_StateEffect : public UObject
{
	GENERATED_BODY()
	
public:

	virtual bool InitObject(AActor* Actor, FName BoneHit);
	virtual void DestroyObject();

	virtual bool CheckStakebleEffect();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakeable = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TOPDOWNSHOOTER_API UTPS_StateEffect_ExecuteOnce : public UTPS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName BoneHit) override;
	void DestroyObject() override;
	//Functions
	virtual void ExecuteOnce();
	//Settings
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute once")
		float Power = 20.0f;
};

UCLASS()
class TOPDOWNSHOOTER_API UTPS_StateEffect_ExecuteTimer : public UTPS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName BoneHit) override;
	void DestroyObject() override;
	//Functions
	virtual void Execute();
	//Settings
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute timer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute timer")
		float GlobalTimer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute timer")
		float RateTime = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute timer")
		bool bIsStuned = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute timer")
		bool bIsHealthBursted = false;

	void CharStunned();
	void BurstHealth();

	FTimerHandle ExecuteTimer;
	FTimerHandle EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute timer")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};