#include "TPSCharHealthComponent.h"
#include "TPSCharHealthComponent.h"
// Fill out your copyright notice in the Description page of Project Settings.


#include "TopDownShooter/Character/TPSCharHealthComponent.h"

void UTPSCharHealthComponent::ChangeCurrentHealth(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//FX
			UE_LOG(LogTemp, Warning, TEXT("UTPSCharHealthComponent::ChangeCurrentHealth Shield < 0"));
		}
	}
	else
	{
		Super::ChangeCurrentHealth(ChangeValue);
	}
}

float UTPSCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldCoolDownTimer, this, &UTPSCharHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
	}
}

void UTPSCharHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(ShieldRecoveryRateTimer, this, &UTPSCharHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTPSCharHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

float UTPSCharHealthComponent::GetShieldValue()
{
	return Shield;
}
