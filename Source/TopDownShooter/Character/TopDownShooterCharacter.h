// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TopDownShooter/FunctionLibrary/Types.h"
#include "TopDownShooter/Items/Weapon/WeaponDefault.h"
#include "TopDownShooter/Character/TPSInventoryComponent.h"
#include "TopDownShooter/Character/TPSCharHealthComponent.h"
#include "TopDownShooter/Interface/TPS_InterfaceGameActor.h"
#include "TopDownShooterCharacter.generated.h"

UCLASS(Blueprintable)
class ATopDownShooterCharacter : public ACharacter, public ITPS_InterfaceGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATopDownShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponentTDS) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTPSCharHealthComponent* CharHealthComponent;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	//Inputs
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputMouseWheelAxis(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float MouseWheelAxis = 0.0f;

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UDecalComponent* CurrentCursor = nullptr;

	// Tick movement
	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementSpeed();

	// Camera slide logic
	// Camera slide variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
		float CameraChangeDistance = 150.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
		float CameraChangeStepDistance = 1.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Slide")
		float CameraCurrentSlideDistance = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
		float HeightCameraMax = 1200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
		float HeightCameraMin = 200.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Slide")
		float CameraDistanceCounter = 0.0f;
	UPROPERTY()
		bool SlideDone = true;
	UPROPERTY()
		bool SlideUp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
		float TimerStep = 0.001f;

	FTimerHandle CameraTimerHandle;
	FTimerHandle RagdollTimer;

	//Camera slide functions
	UFUNCTION(BlueprintCallable)
		void CameraSlideSmooth();
	UFUNCTION(BlueprintCallable)
		void CameraSlideSmoothTimer();

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement flags")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement flags")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement flags")
		bool WalkEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadAnims;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		UAnimMontage* StunAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPS_StateEffect> AbilityEffect;
	
	FTimerHandle SprintTimerHandle;
	UFUNCTION()
		void SprintBoostTimer();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float CurrentSpeedSprint = 600.0f;

	//Weapon
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	AWeaponDefault* CurrentWeapon = nullptr;
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);

		void WeaponReloadStart(UAnimMontage* Anim, AActor* MagazineDrop);
	UFUNCTION()
		void WeaponFire(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFire_BP(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)

		void WeaponReloadStart_BP(UAnimMontage* Anim);

		void WeaponReloadStart_BP(UAnimMontage* Anim, AActor* MagazineDrop);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Logic")
		bool IsFireing = false;
	UFUNCTION()
		void ChamberSpawn();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	//UPROPERTY(BlueprintReadOnly)
		int32 CurrentIndexWeapon = 0;

	//SwichWeapon
	UFUNCTION()
		void TrySwichNextWeapon();
	UFUNCTION()
		void TrySwichPreviosWeapon();
	UFUNCTION()
		bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);

	//DropWeapon
	UFUNCTION()
	void DropCurrentWeapon();

	//Ability func
	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}

	//Effect
	TArray<UTPS_StateEffect*> Effects;
	UFUNCTION(BlueprintCallable)
		void TryEnableEffect(TSubclassOf<UTPS_StateEffect> EffectToEnable);

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTPS_StateEffect* RemoveEffect) override;
	void AddEffect(UTPS_StateEffect* NewEffect) override;
	//EndInterface

	UFUNCTION()
	void CharDead();
	UFUNCTION()
	void EnableRagdoll();

	UFUNCTION(BlueprintNativeEvent)
	void CharDead_BP();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetAliveStatus();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};
