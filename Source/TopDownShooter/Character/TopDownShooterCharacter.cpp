// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TopDownShooter/Character/TPSInventoryComponent.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TopDownShooter/Game/TDSGameInstance.h"
#include "TopDownShooter/Items/Weapon/ProjectileDefault.h"
#include "Engine/World.h"

void ATopDownShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial /*&& !CurrentWeapon->WeaponReloading*/ )
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), 
			CursorMaterial, CursorSize, FVector(0));
	}
}

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTPSCharHealthComponent>(TEXT("CharHealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATopDownShooterCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATopDownShooterCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);

	CameraSlideSmooth();
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* InputComponentTDS)
{
	Super::SetupPlayerInputComponent(InputComponentTDS);

	InputComponentTDS->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	InputComponentTDS->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);
	InputComponentTDS->BindAxis(TEXT("MouseWheel"), this, &ATopDownShooterCharacter::InputMouseWheelAxis);

	InputComponentTDS->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::InputAttackPressed);
	InputComponentTDS->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::InputAttackReleased);

	InputComponentTDS->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATopDownShooterCharacter::TryReloadWeapon);

	InputComponentTDS->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwichNextWeapon);
	InputComponentTDS->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TrySwichPreviosWeapon);

	InputComponentTDS->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::TryAbilityEnabled);
	InputComponentTDS->BindAction(TEXT("DropWeapon"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	InputComponentTDS->BindKey(HotKeys[1], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<1>);
	InputComponentTDS->BindKey(HotKeys[2], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<2>);
	InputComponentTDS->BindKey(HotKeys[3], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<3>);
	InputComponentTDS->BindKey(HotKeys[4], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<4>);
	InputComponentTDS->BindKey(HotKeys[5], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<5>);
	InputComponentTDS->BindKey(HotKeys[6], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<6>);
	InputComponentTDS->BindKey(HotKeys[7], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<7>);
	InputComponentTDS->BindKey(HotKeys[8], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<8>);
	InputComponentTDS->BindKey(HotKeys[9], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<9>);
	InputComponentTDS->BindKey(HotKeys[0], IE_Pressed, this, &ATopDownShooterCharacter::TKeyPressed<0>);
}

void ATopDownShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownShooterCharacter::InputMouseWheelAxis(float Value)
{
	MouseWheelAxis = Value;
}

void ATopDownShooterCharacter::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
}

void ATopDownShooterCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

UDecalComponent* ATopDownShooterCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		/*if (MovementState == EMovementState::SprintRun_State) // sprint only forward
		{
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();
			SetActorRotation((FQuat(myRotator)));
		}
		else
		{*/
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::AimWalk_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::SprintRun_State:
					break;
				default:
					break;
				}
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
			}
		}
	}
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	default:
		break;
	};

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownShooterCharacter::ChangeMovementSpeed()
{
	CurrentSpeedSprint = 600.0f;

	if (!AimEnabled && !WalkEnabled && !SprintRunEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;

			GetWorld()->GetTimerManager().SetTimer(SprintTimerHandle,
				this, &ATopDownShooterCharacter::SprintBoostTimer,
				0.01, true);

			//MovementState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && AimEnabled && !SprintRunEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !AimEnabled && !SprintRunEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (AimEnabled && !WalkEnabled && !SprintRunEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}

	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATopDownShooterCharacter::CameraSlideSmooth()
{
	if (SlideDone)
	{
		if (MouseWheelAxis != 0)
		{
			if (MouseWheelAxis < 0)
			{
				if (CameraCurrentSlideDistance <= HeightCameraMax)
				{
					SlideUp = true;
					SlideDone = false;

					GetWorld()->GetTimerManager().SetTimer(CameraTimerHandle, 
						this, &ATopDownShooterCharacter::CameraSlideSmoothTimer, 
						TimerStep, true);
				}
			}
			else
			{
				if (CameraCurrentSlideDistance >= HeightCameraMin)
				{
					SlideUp = false;
					SlideDone = false;

					GetWorld()->GetTimerManager().SetTimer(CameraTimerHandle, 
						this, &ATopDownShooterCharacter::CameraSlideSmoothTimer, 
						TimerStep, true);
				}
			}
		}
	}
}

void ATopDownShooterCharacter::CameraSlideSmoothTimer()
{
	if (SlideUp)
	{
		CameraBoom->TargetArmLength += CameraChangeStepDistance;
		CameraDistanceCounter += CameraChangeStepDistance;
	}
	else
	{
		CameraBoom->TargetArmLength -= CameraChangeStepDistance;
		CameraDistanceCounter += CameraChangeStepDistance;
	}

	if (CameraDistanceCounter >= CameraChangeDistance)
	{
		if (SlideUp)
		{
			CameraCurrentSlideDistance += CameraChangeDistance;
		}
		else
		{
			CameraCurrentSlideDistance -= CameraChangeDistance;
		}
		CameraDistanceCounter = 0.0f;
		SlideDone = true;
		GetWorld()->GetTimerManager().ClearTimer(CameraTimerHandle);
	}
}

void ATopDownShooterCharacter::SprintBoostTimer()
{
	if (CurrentSpeedSprint < 800)
	{
		CurrentSpeedSprint += 1;
		GetCharacterMovement()->MaxWalkSpeed = CurrentSpeedSprint;
	}
	else
	{
		MovementState = EMovementState::SprintRun_State;
		GetCharacterMovement()->MaxWalkSpeed = CurrentSpeedSprint;
		GetWorld()->GetTimerManager().ClearTimer(SprintTimerHandle);
	}
}

void ATopDownShooterCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
		IsFireing = true;

		if (myWeapon->WeaponFiring)
		{
			if (AimEnabled)
			{
				PlayAnimMontage(myWeapon->WeaponSetting.AnimationWeaponInfo.AnimCharFireAiming);
			}
			else
			{
				WeaponFire(myWeapon->WeaponSetting.AnimationWeaponInfo.AnimCharFire);
				//PlayAnimMontage(myWeapon->WeaponSetting.AnimCharFire);
			}
		}
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon - NULL"));
}

AWeaponDefault* ATopDownShooterCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

int32 ATopDownShooterCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

void ATopDownShooterCharacter::InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;

					myWeapon->UpdateStateWeapon(MovementState);

					//debug remove
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;

					myWeapon->WeaponSetting.AdditionalWeaponInfo = WeaponAdditionalInfo;

					//if (InventoryComponent)
					
					CurrentIndexWeapon = NewCurrentIndexWeapon;
					

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATopDownShooterCharacter::WeaponReloadEnd);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if (InventoryComponent)
					{
						InventoryComponent->OnWeaponAmmoAvalible.Broadcast(myWeapon->WeaponSetting.WeaponType);
					}
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("ATopDownShooterCharacter::InitWeapon - weapon not found in table - NULL"));
			}
		}
	}
}

void ATopDownShooterCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}
	}
	WeaponReloadStart(CurrentWeapon->WeaponSetting.AnimationWeaponInfo.AnimCharReload);
}

void ATopDownShooterCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATopDownShooterCharacter::WeaponFire(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	WeaponFire_BP(Anim);
}

void ATopDownShooterCharacter::WeaponFire_BP_Implementation(UAnimMontage* Anim)
{
	
}

void ATopDownShooterCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeAmmo(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		InventoryComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);
}

void ATopDownShooterCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// to BP
}

void ATopDownShooterCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// to BP
}

void ATopDownShooterCharacter::ChamberSpawn()
{
	
}

void ATopDownShooterCharacter::TrySwichNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaponToIndexNOP(CurrentIndexWeapon + 1, OldIndex, OldInfo, true);
		}
	}
}

void ATopDownShooterCharacter::TrySwichPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;

		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}
		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexNOP(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}

bool ATopDownShooterCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->WeaponSetting.AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	return bIsSuccess;
}



void ATopDownShooterCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropWeapon ItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
	}
}

void ATopDownShooterCharacter::TryAbilityEnabled()
{
	if (AbilityEffect) //cooldown todo
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

void ATopDownShooterCharacter::TryEnableEffect(TSubclassOf<UTPS_StateEffect> EffectToEnable)
{
	if (EffectToEnable)
	{
		UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(this, EffectToEnable);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

EPhysicalSurface ATopDownShooterCharacter::GetSurfaceType()
{
	EPhysicalSurface result = EPhysicalSurface::SurfaceType_Default;

	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}

	return result;
}

TArray<UTPS_StateEffect*> ATopDownShooterCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATopDownShooterCharacter::RemoveEffect(UTPS_StateEffect* RemoveEffect)
{

	Effects.Remove(RemoveEffect);
}

void ATopDownShooterCharacter::AddEffect(UTPS_StateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

void ATopDownShooterCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadAnims.Num());

	if (DeadAnims.IsValidIndex(rnd) && DeadAnims[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadAnims[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadAnims[rnd]);
	}

	bIsAlive = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}

	UnPossessed();

	// Timer ragdoll
	GetWorldTimerManager().SetTimer(RagdollTimer, this, &ATopDownShooterCharacter::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);

	AttackCharEvent(false);

	CharDead_BP();
}

void ATopDownShooterCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
	GetWorld()->GetTimerManager().ClearTimer(RagdollTimer);
}

void ATopDownShooterCharacter::CharDead_BP_Implementation()
{
	//BP only
}

bool ATopDownShooterCharacter::GetAliveStatus()
{
	return bIsAlive;
}

float ATopDownShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (bIsAlive)
	{
		CharHealthComponent->ChangeCurrentHealth(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType()); // NAME_None - bone for radial damage
		}
	}
	
	return ActualDamage;
}
